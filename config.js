'use strict';

module.exports = {
  url: 'https://jamilelima.gitlab.io',
  title: 'Jamile Lima',
  subtitle: 'Software Developer',
  copyright: '2019 © All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: '',
  menu: [
    {
      label: 'Articles',
      path: '/'
    },
    {
      label: 'About me',
      path: '/pages/about'
    },
    {
      label: 'Contact me',
      path: '/pages/contacts'
    }
  ],
  author: {
    name: 'Jamile Lima',
    photo: '/photo.jpg',
    bio: '',
    contacts: {
      email: 'jamilelima@protonmail.com',
      telegram: '#',
      twitter: '#',
      github: 'jamilelima',
      rss: '#',
      vkontakte: '#'
    }
  }
};
